AntiFeatures:NonFreeNet
Categories:Multimedia,Connectivity
License:MIT
Web Site:
Source Code:https://github.com/zouroboros/filmchecker
Issue Tracker:https://github.com/zouroboros/filmchecker/issues

Auto Name:FilmChecker
Summary:Checks the status of film orders
Description:
Checks the status of film orders. Currently only the Rossmann and DM (german
drugstores) are supported.
.

Repo Type:git
Repo:https://github.com/zouroboros/filmchecker.git

Build:1.0.0,1
    commit=1.0.0
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.0
Current Version Code:1
