Categories:Internet
License:GPLv3+
Web Site:
Source Code:https://github.com/qqq3/good-weather
Issue Tracker:https://github.com/qqq3/good-weather/issues
Changelog:https://raw.githubusercontent.com/qqq3/good-weather/HEAD/CHANGELOG
Bitcoin:16rSzigYf3roULrPuMFe6mb5xxHE3iZspM

Auto Name:Good Weather
Summary:Display weather information
Description:
Show current weather information from [http://openweathermap.org/
OpenWeatherMap].

Support of English, Russian, Polish and German languages.

Features:

* Find current location with GPS or select from the list
* Many locations
* Notifications
* Support different measuring units
* Without ads
.

Repo Type:git
Repo:https://github.com/qqq3/good-weather.git

Build:2.1,6
    disable=pre-release sdk
    commit=v2.1
    subdir=app
    init=echo 'task wrapper(type: Wrapper) { gradleVersion = "2.14.1" }' >> ../build.gradle
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.1
Current Version Code:6
