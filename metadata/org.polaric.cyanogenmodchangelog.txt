Categories:Internet
License:GPLv3+
Web Site:https://git.polaric.org/polaric/CMLog/blob/HEAD/README.md
Source Code:https://git.polaric.org/polaric/CMLog/tree/master
Issue Tracker:http://bugs.polaric.org/projects/cmlog

Auto Name:CMLog
Summary:View CM changelog
Description:
View the latest CyanogenMod changelogs for all of your favorite devices.

Features

* Multi-version and multi-device support.
* A beautiful material designed UI.
* Lightweight and fast.
* No permissions or phoning home.
.

Repo Type:git
Repo:https://git.polaric.org/polaric/CMLog.git

Build:5.3,57
    commit=d545f76bc17f3b14a886e9bf01aeaaf9d1bda137
    subdir=app
    gradle=yes

Build:5.3,58
    commit=e78af38d1718148a399fa0a758e43542774861a7
    subdir=app
    gradle=yes

Build:5.3,59
    commit=b73f4aa98e81950d5a4ec37793ce3fa9f994ea39
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:5.3
Current Version Code:59
